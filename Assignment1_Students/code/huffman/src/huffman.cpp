#include "huffman.h"
#include <iostream>
#include <algorithm>
#include <array>
#include <vector>

using std::cout;
using std::sort;
using std::array;
using std::vector;
using std::endl;

/*
convert 4 byte unsigned int to 4 char array
*/
unsigned char* int_2_char(unsigned int n)
{
    unsigned char *bytes = (unsigned char*) malloc(sizeof(unsigned char) * 4);
    bytes[0] = (n >> 24) & 0xFF;
    bytes[1] = (n >> 16) & 0xFF;
    bytes[2] = (n >> 8) & 0xFF;
    bytes[3] = n & 0xFF;
    return bytes;
}

/*
convert 4 char array to 4 byte unsigned int
*/
unsigned int char_2_int(unsigned char* char_array)
{
    return (char_array[0] << 24) + (char_array[1] << 16) + (char_array[2] << 8) + (char_array[3]);
}

/**
 * TODO Complete this function
 **/
int huffman_encode(const unsigned char *bufin,
						  unsigned int bufinlen,
						  unsigned char **pbufout,
						  unsigned int *pbufoutlen)
{
	// initialize freq count array
    array<unsigned int, 2> histogram[256] = {{ 0 }};
    for (int i = 0; i < 256; i++) {
        histogram[i][0] = 0;
        histogram[i][1] = i; // {count, char value}
    }

    // iterate through input, counting occurences
    unsigned char char_value;
    for (int n = 0; n < bufinlen; n++)  {
        char_value = bufin[n];
        histogram[char_value][0] = histogram[char_value][0] + 1;
    }

    // sort histogram
    sort(histogram, histogram + 256); //using default to sort 2D array by comparing first elements

    // find first nonzero element
    unsigned int first_element = 256;
    for (int i = 0; i < 256; i++) {
        if (histogram[i][0] != 0) {
            first_element = i;
            break;
        }
    }
    unsigned char len_alphabet = 256 - first_element;

    cout << "Sorted histogram\n";
    for (int j = first_element; j < 256; j++) {
        cout << histogram[j][0] << ", " << histogram[j][1] << "\n";
    }

    // set up to create tree
    unsigned char num_internal_nodes = len_alphabet;
    vector<vector<int>> internal_nodes; //format is <count, own_symbol, left_child_symbol, right_child_symbol>, ...
    vector<vector<int>> leaf_nodes; //format is <count, symbol>, ...
    vector<vector<int>> priority_queue; //format is <count, symbol>, ...

    // populate leaf nodes and priority queue
    for (int j = first_element; j < 256; j++) {
        vector<int> temp_node;
        temp_node.push_back(histogram[j][0]);
        temp_node.push_back(histogram[j][1]);
        leaf_nodes.push_back(temp_node);
        priority_queue.push_back(temp_node);
    }

    // loop through priority queue
    int internal_node_symbol_counter = 256; //always greater than ASCII
    while (priority_queue.size() > 1) {
        // cout << "looping through priority queue, " << internal_node_symbol_counter << "\n";
        sort(priority_queue.begin(), priority_queue.end(), [](const vector<int>& a, const vector<int>& b) {return a[0] > b[0];}); //sort in descending order to use pop_back()
        vector<int> dequeue0 = priority_queue[priority_queue.size() - 1];
        vector<int> dequeue1 = priority_queue[priority_queue.size() - 2];
        vector<int> temp_internal_node{ dequeue0[0] + dequeue1[0], internal_node_symbol_counter, dequeue0[1], dequeue1[1] };
        internal_node_symbol_counter = internal_node_symbol_counter + 1; //increment
        priority_queue.pop_back();
        priority_queue.pop_back();
        internal_nodes.push_back(temp_internal_node);
        priority_queue.push_back(temp_internal_node);
    }
    internal_nodes.push_back(priority_queue.back()); //add root

    // debug, print out internals
    for (int i = 0; i < (internal_node_symbol_counter - 256); i++) {
        for (auto it = internal_nodes[i].begin(); it != internal_nodes[i].end(); it++) {
            cout << *it << " ";
        }
        cout << endl;
    }

    //traverse tree from root, creating code
    vector<vector<int>> code; //format is <symbol, code0, code1, ...>
    cout << "num_internal_nodes " << internal_nodes.size() << " vs " << int(num_internal_nodes) << endl;
    for (int i = 0; i < leaf_nodes.size(); i++) {
        vector<int> letter_code;
        vector<int> current_node = leaf_nodes[i];
        letter_code.push_back(current_node[1]); //store leaf's letter's symbol
        while (current_node != priority_queue.back()) { //haven't found root yet
            for (int j = 0; j < internal_nodes.size(); j++) {

                if (internal_nodes[j][2] == current_node[1]) {
                    current_node = internal_nodes[j];
                    letter_code.push_back(0);
                    break;
                }
                if (internal_nodes[j][3] == current_node[1]) {
                    current_node = internal_nodes[j];
                    letter_code.push_back(1);
                    break;
                }
            }
            //debug
            // if (current_node[1] == 295) {
            //     cout << "current_node:";
            //     for (auto it = current_node.begin(); it != current_node.end(); it++) {
            //         cout << " " << *it;
            //     }
            //     cout << "\nroot_node:";
            //     for (auto it = priority_queue.back().begin(); it != priority_queue.back().end(); it++) {
            //         cout << " " << *it;
            //     }
            //     cout << "\n";
            // }
            // cout << "current_node symbol is: " << current_node[1] << endl;
        }
        code.push_back(letter_code);
    }
    sort(code.begin(), code.end(), [](const vector<int>& a, const vector<int>& b) {return a[0] < b[0];}); //sort code to alphabetic order

    // debug, print out code
    for (auto iter1 = code.begin(); iter1 != code.end(); iter1++) {
        for (auto iter2 = (*iter1).begin(); iter2 != (*iter1).end(); iter2++) {
            cout << *iter2 << " ";
        }
        cout << endl;
    }

    // create output
    unsigned int alphabet_size = 0; //format is <symbol, size, code0, code1, ...
    for (auto iter1 = code.begin(); iter1 != code.end(); iter1++) {
        (*iter1).insert((*iter1).begin() + 1,(*iter1).size() - 1); //add size in bits as second element
        alphabet_size = alphabet_size + (*iter1).size();
    }

    cout << "alphabet_size: " << alphabet_size << endl << endl;

    // // testing converters
    // cout << "input to converters: " << alphabet_size << endl;
    // cout << "output after both converters: " << char_2_int(int_2_char(alphabet_size)) << endl;

    //put code in output
    vector<unsigned char> output_stream;
    unsigned char* temp_array = int_2_char(alphabet_size);
    for (int n = 0; n < 4; n++) {
        output_stream.push_back(temp_array[n]);
    }
    for (auto iter1 = code.begin(); iter1 != code.end(); iter1++) {
        for (auto iter2 = (*iter1).begin(); iter2 != (*iter1).end(); iter2++) {
            output_stream.push_back(*iter2);
        }
    }
    
    // iterate through input and encode, then output
    unsigned char char_value_to_encode;
    unsigned int char_stream_size = 0;
    for (int n = 0; n < 4; n++) {
        output_stream.push_back(0); //dummy values, to be filled after counting size
    }

    for (int n = 0; n < bufinlen; n++) { //iterate over input textfile
        char_value_to_encode = bufin[n];
        for (auto iter1 = code.begin(); iter1 != code.end(); iter1++) { //lookup value in code
            if ((*iter1)[0] == char_value_to_encode) {
                for (int m = 2; m < (*iter1).size(); m++) {
                    output_stream.push_back((*iter1)[m]); //writing out code
                    char_stream_size = char_stream_size + 1;
                }
                break;
            }
        }
    }

    temp_array = int_2_char(char_stream_size);
    for (int n = 0; n < 4; n++) {
        output_stream[4+alphabet_size+n] = temp_array[n];
    }

    unsigned char* pbufout_inter = &output_stream.front();
    pbufout = &pbufout_inter;
    unsigned int bufoutlen = output_stream.size();
    *pbufoutlen = bufoutlen;

    // cout << "pbufout:";
    // for (int p = 0; p < 200; p++) {
    //     cout << " " << (unsigned char)(*pbufout)[p];
    //     // cout << " " << p;
    // }
    // cout << endl;

    // cout << "char_stream_size: " << char_stream_size << endl;
    // cout << endl;

    return 0;
}


/**
 * TODO Complete this function
 **/
int huffman_decode(const unsigned char *bufin,
						  unsigned int bufinlen,
						  unsigned char **pbufout,
						  unsigned int *pbufoutlen)
{
	// input is of form len_alphabet, <alphabet array>, len_bitstream, <bitstream>

    cout << "==BEGINNING DECODE==" << endl;

    unsigned int alphabet_size = char_2_int(bufin);
    cout << "alphabet_size: " << alphabet_size << endl;

    // iterate through input, counting occurences
    unsigned char char_value;
    for (int n = 0; n < alphabet_size; n++)  {
        char_value = bufin[4+n];
        histogram[char_value][0] = histogram[char_value][0] + 1;
    }

    return 0;
}
