#include "main.h"


using namespace cv;
using namespace std;


Mat LUT_w;
Mat LUT_h;


// Helper function
float sf(int in){
	if (in == 0)
		return 0.70710678118; // = 1 / sqrt(2)
	return 1.;
}

// Initialize LUT
void initDCT(int WIDTH, int HEIGHT)
{
    // pre-calculate scale factor, include in LUTs
    float scale = 2./sqrt(HEIGHT*WIDTH);
    float sqrt_scale = sqrt(scale);

	// pre-calculate all the cosines
    LUT_w.create(WIDTH, WIDTH, CV_32FC1);
    for(int x = 0; x < LUT_w.rows; x++)
    {
        float* Mx = LUT_w.ptr<float>(x);
        for(int i = 0; i < LUT_w.cols; i++)
        {
            Mx[i] = cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x);
            Mx[i] = sqrt_scale * sf(x) * Mx[i];
        }
    }

    LUT_h.create(HEIGHT, HEIGHT, CV_32FC1);
    for(int y = 0; y < LUT_h.rows; y++)
    {
        float* My = LUT_h.ptr<float>(y);
        for(int j = 0; j < LUT_h.cols; j++)
        {
            My[j] = cos(M_PI/((float)WIDTH)*(j+1./2.)*(float)y);
            My[j] = sqrt_scale * sf(y) * My[j];
        }
    }
}

// // Baseline: O(N^4)
// // naive 4x for loop
// Mat student_dct(Mat input)
// {
// 	const int HEIGHT = input.rows;
// 	const int WIDTH  = input.cols;

// 	// float scale = 2./sqrt(HEIGHT*WIDTH); //move to initDCT()

// 	Mat result = Mat(HEIGHT, WIDTH, CV_32FC1);

// 	// Note: Using pointers is faster than Mat.at<float>(x,y)
// 	// Try to use pointers for your LUT as well
// 	float* result_ptr = result.ptr<float>();
// 	float* input_ptr  = input.ptr<float>();
//     float* LUT_h_ptr = LUT_h.ptr<float>();
//     float* LUT_w_ptr  = LUT_w.ptr<float>();

// 	for(int x = 0; x < HEIGHT; x++)
// 	{
// 		for(int y = 0; y < WIDTH; y++)
// 		{
// 			float value = 0.f;

// 			for(int i = 0; i < HEIGHT; i++)
// 			{
// 				for(int j = 0; j < WIDTH; j++)
// 				{
// 					value += input_ptr[i * WIDTH + j]
// 						// TODO
// 						// --- Replace cos calculation by LUT ---
// 						// * cos(M_PI/((float)HEIGHT)*(i+1./2.)*(float)x)
// 						// * cos(M_PI/((float)WIDTH)*(j+1./2.)*(float)y);
//                         * LUT_h_ptr[x * HEIGHT + i]
//                         * LUT_w_ptr[y * WIDTH + j];
// 				}
// 			}
// 			// TODO
// 			// --- Incorporate the scale in the LUT coefficients ---
// 			// --- and remove the line below ---
// 			// value = scale * sf(x) * sf(y) * value;

// 			result_ptr[x * WIDTH + y] = value;
// 		}
// 	}

// 	return result;
// }


// *****************
//   Hint
// *****************
//
// DCT as matrix multiplication

// helper func to multiply two matrices
// assumes multiplication is valid (inner dimensions agree, same type of float)
Mat naive_matrix_mult(Mat A, Mat B)
{
    Mat C;
    C = Mat::zeros(A.rows, B.cols, A.type());
    const float* A_ptr = A.ptr<float>();
    const float* B_ptr = B.ptr<float>();
    float* C_ptr = C.ptr<float>();
    for(int i = 0; i < A.rows; i++)
    {
        for(int j = 0; j < B.cols; j++)
        {
            for(int k = 0; k < A.cols; k++)
            {
                C_ptr[i * A.rows + j] += A_ptr[i * A.rows + k] * B_ptr[k * A.cols + j];
            }
        }
    }
    return C;
}

Mat block_matrix_mult(Mat A, Mat B)
{
    Mat D;
    D = Mat::zeros(A.rows, B.cols, A.type());
    // const float* A_ptr = A.ptr<float>();
    // const float* B_ptr = B.ptr<float>();
    // float* D_ptr = D.ptr<float>();
    int num_partitions = 3; //i.e., 2 partitions in a 4x4 gives 4 2x2 matrices, assumes this gives an integer size partitions
    int partition_size = A.rows/num_partitions; //assuming square matrix
    for (int i = 0; i < num_partitions; i++) //vertical iteration through A
    {
        for (int j = 0; j < num_partitions; j++) //horizontal iteration through A
        {
            Mat intermed_result = D(Rect(j*partition_size, i*partition_size, partition_size, partition_size));
            for (int k = 0; k < num_partitions; k++) //vertical iteration through B
            {
                //now get blocks, pass to naive with smaller blocks
                const Mat intermed_1 = A(Rect(k*partition_size, i*partition_size, partition_size, partition_size)).clone();
                const Mat intermed_2 = B(Rect(j*partition_size, k*partition_size, partition_size, partition_size)).clone();
                intermed_result = intermed_result + naive_matrix_mult(intermed_1, intermed_2);
            }
        }
    }
    return D;
}


Mat student_dct(Mat input)
{
	// -- Works only for WIDTH == HEIGHT
	assert(input.rows == input.cols);
	
	// -- Matrix multiply with OpenCV
	// Mat output = LUT_w * input * LUT_w.t();

    // naive matrix mult
    // Mat result_i = naive_matrix_mult(LUT_w, input);
    // Mat output = naive_matrix_mult(result_i, LUT_w.t());

    // block matrix mult version
    Mat result_i = block_matrix_mult(LUT_w, input);
    Mat output = block_matrix_mult(result_i, LUT_w.t());

	// TODO
	// Replace the line above by your own matrix multiplication code
	// You can use a temp matrix to store the intermediate result

	return output;
}






